package day1125.web;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class WebViewApp extends Application{
   WebView webView;
	
	public void start(Stage stage) throws Exception {
		//��Ʃ�� �����ϱ�
		webView =new WebView();
		webView.getEngine().load("https://www.youtube.com/watch?v=COBEibKYjFE");
		webView.setPrefSize(500, 500);
		
		showWindow(stage, webView);
	}
	public void showWindow(Stage stage,Parent parent) {
		Scene s=new Scene(parent);
		stage.setScene(s);
		stage.setWidth(500);
		stage.setHeight(500);
		stage.show();
	}
	public static void main(String[] args) {
		launch(args);

	}

}
