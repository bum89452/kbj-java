/*코로나현황을 지역별로 표현해보자*/
package day1125.chart;

import java.util.Locale.Category;

import com.sun.media.jfxmedia.events.NewFrameEvent;

import javafx.application.Application;
import javafx.geometry.Side;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

public class BarchartApp extends Application{
     BarChart bar;
     CategoryAxis x;//x툭으로 사용할 예정
     NumberAxis y;//y축으로 사용할 객체
	
	public void start(Stage stage) throws Exception {
		x=new CategoryAxis();
		y= new NumberAxis();
		
		//x,y축에 라벨달기
		x.setLabel("Area");
		y.setLabel("Peple");
		
		//데이터구성!!XYChartSeries이용하자!!
        XYChart.Series s =new XYChart.Series();
        s.setName("아시아");
        s.getData().add(new XYChart.Data("20만명",20));
        
        XYChart.Series s2 =new XYChart.Series();
        s.setName("유럽");
        s.getData().add(new XYChart.Data("68만명",68));
		
        
        XYChart.Series s3 =new XYChart.Series();
        s.setName("북미");
        s.getData().add(new XYChart.Data("300만명",300));
		
        bar=new BarChart(x, y);
		bar.setLegendSide(Side.RIGHT);
		
		bar.getData().addAll(s,s2,s3);
		
		
		showWindow(stage, bar);
	}
	
	public void showWindow(Stage stage,Parent parent) {
		Scene s=new Scene(parent);
		stage.setScene(s);
		stage.setWidth(500);
		stage.setHeight(500);
		stage.show();
	}

	public static void main(String[] args) {
	  launch(args);

	}

}
