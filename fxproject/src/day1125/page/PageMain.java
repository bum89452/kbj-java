/*화면전환*/
package day1125.page;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class PageMain extends Application{
	String[] naviTitle = {"Main","Shopping","Member","Login"};
	Pane[] page=new Pane[naviTitle.length];
	Button[] btn =new Button[4];
	ToolBar bar;//네비게이션을 포함하는 메뉴바
	BorderPane border;//전체레이아웃
	
	public void start(Stage stage) throws Exception {
		border =new BorderPane();
		
		for(int i=0;i<page.length;i++) {
			page[i]=(Pane)FXMLLoader.load(this.getClass().getClassLoader().getResource("day1125/page/"+naviTitle[i].toLowerCase()+".fxml"));
		}
		
		
		//네비게이션 만들기
		for(int i=0;i<btn.length;i++) {
			btn[i]=new Button(naviTitle[i]);
			
			btn[i].setId(Integer.toString(i));//버튼에 정보심기!!
		    
			//버튼에 액션이벤트연결
			btn[i].setOnAction((e)->{
				Button bt =(Button)e.getSource();
				
				System.out.println(bt.getId()+" 눌렀나요?");//누른 버튼의 i가 함께 출력되도록
				
				
				
				showPage(page[Integer.parseInt(bt.getId())]);
			});
		}
		
		
		bar=new ToolBar(btn[Pages.Main.ordinal()],btn[Pages.Shopping.ordinal()],btn[Pages.Member.ordinal()],btn[Pages.Login.ordinal()]);//툴바생성
		bar.setBackground(new Background(new BackgroundFill(Color.BLACK,null,null)));
		border.setTop(bar);//생성된 바를 top영역에 부착!!
	
		showPage(page[Pages.Shopping.ordinal()]);
		
		showWindow(stage, border);
	}
	
	//페이지 전환메서드 정의
	public void showPage(Pane p){
		border.getChildren().removeAll(border.getChildren());//싹~~~지워짐
		
		
		border.setTop(bar);
		border.setCenter(p);//xml에서도르한 컨테이너  setVible(True),setVible(false);로불가
	}
	
	public void showWindow(Stage stage,Parent parent) {
		Scene s=new Scene(parent);
		stage.setScene(s);
		stage.setWidth(800);
		stage.setHeight(700);
		stage.show();
	}
	public static void main(String[] args) {
      launch(args);
	}

}
